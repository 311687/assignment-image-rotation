#ifndef FILE_UTIL
#define FILE_UTIL

#include <stdbool.h>
#include <stdio.h>

bool openf(FILE** file, const char* name, const char* mode);

bool closef(FILE** file);

#endif
