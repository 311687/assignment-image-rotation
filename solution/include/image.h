#ifndef IMAGE
#define IMAGE

#include <stdint.h>
#include <stdio.h>

struct image {
    size_t width;
    size_t height;
    struct pixel* pixels;
};

struct __attribute__((__packed__))pixel {
    uint8_t b, g, r;
};

struct image image_create(size_t width, size_t height);

void image_delete(struct image img);

#endif
