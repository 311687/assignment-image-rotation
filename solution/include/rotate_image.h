#ifndef ROTATE_IMAGE
#define ROTATE_IMAGE

#include "image.h"

struct image rotate(struct image const* source);

#endif
