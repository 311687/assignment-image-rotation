#include "../include/bmp_util.h"

#include <assert.h>
#include <malloc.h>
#include <stdio.h>

#define BMP_TYPE 0x4D42
#define BMP_PLANES 1
#define BMP_HEADER_SIZE 40

struct __attribute__((packed)) bmp_header {
	uint16_t bfType;
	uint32_t bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
};

static enum read_status read_header(FILE* in, struct bmp_header* header) {
	if (fread(header, sizeof(struct bmp_header), 1, in) == 1) {
		return READ_OK;
	}
	else{
		return READ_INVALID_HEADER;
	}
}

static enum read_status read_pixels(struct image* img, FILE* in, uint8_t padding) {
	const size_t width = img->width;
	const size_t height = img->height;

	struct pixel* pixels = malloc(width * height * sizeof(struct pixel));

	for (size_t i = 0; i < height; i++) {
		if (pixels + i * width != 0){
			if (fread(pixels + i * width, sizeof(struct pixel), width, in) != width){
				free(pixels);
				return READ_PIXELS_ERROR;
			}
			if(fseek(in, padding, SEEK_CUR) != 0){
				free(pixels);
				return READ_PIXELS_ERROR;
			}
		}
	}
	img->pixels = pixels;
	return READ_OK;
}



static uint8_t get_padding(uint32_t width) {
	return width % 4;
}



enum read_status from_bmp(FILE* in, struct image* img) {
	struct bmp_header header = { 0 };
	if (read_header(in, &header) == READ_INVALID_HEADER) {
		return READ_INVALID_SIGNATURE;
	}
	img->width = header.biWidth;
	img->height = header.biHeight;
	return read_pixels(img, in, get_padding(header.biWidth));
}

static uint32_t get_img_size(const struct image* img){
	return (sizeof(struct pixel) * img->width + get_padding(img->width)) * img->height;
}

static struct bmp_header create_header(const struct image* img) {
	const uint32_t img_size = get_img_size(img);
	struct bmp_header header = {
		.bfType = BMP_TYPE,
		.bfileSize = sizeof(struct bmp_header) + img_size,
		.bfReserved = 0,
		.bOffBits = sizeof(struct bmp_header),
		.biSize = BMP_HEADER_SIZE,
		.biWidth = img->width,
		.biHeight = img->height,
		.biPlanes = BMP_PLANES,
		.biBitCount = 24,
		.biCompression = 0,
		.biSizeImage = img_size,
		.biXPelsPerMeter = 0,
		.biYPelsPerMeter = 0,
		.biClrUsed = 0,
		.biClrImportant = 0,

	};
	return header;
}



static enum write_status write_image(struct image const* img, FILE* out, uint8_t padding) {
	const size_t width = img->width;
	const size_t height = img->height;
	uint64_t zero = 0;
	for (size_t i = 0; i < height; ++i) {
		if(fwrite(img->pixels + i * width, sizeof(struct pixel), width, out) != width){
			return WRITE_ERROR;
		}
		if(fwrite(&zero, 1, padding, out) != padding){
			return WRITE_ERROR;
		}
	}
	return WRITE_OK;
}




enum write_status to_bmp(FILE* out, struct image* img) {
	struct bmp_header header = create_header(img);
	if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
		return WRITE_ERROR;
	}
	uint8_t padding = get_padding(header.biWidth);
	return write_image(img, out, padding);
}
