#include "../include/rotate_image.h"
#include "../include/bmp_util.h"
#include "../include/file_util.h"
#include "../include/image.h"

#include <assert.h>
#include <malloc.h>

int main(int args, char** argv) {
	FILE* in = NULL;
	FILE* out = NULL;

	if (args != 3) 
	{
		fprintf( stderr, "Не введен входной или выходной файл!");
		return 1;
	}
	char* file_name_input = argv[1];
	char* file_name_output = argv[2];

	if (openf(&in, file_name_input, "rb")==false || openf(&out, file_name_output, "wb")==false) {
		fprintf( stderr, "Ошбика в чтении файла!");
		return 1;
	}
	struct image image = { 0 };
	if (from_bmp(in, &image) != READ_OK) {
		fprintf( stderr, "Ошбика в чтении файла!");
		image_delete(image);
		return 1;
	}
	struct image rotated_image = rotate(&image);
	if (to_bmp(out, &rotated_image) != WRITE_OK) {
		fprintf( stderr, "Ошибка записи в файл!");
		image_delete(rotated_image);
		image_delete(image);
		return 1;
	}
	if (!closef(&in) || !closef(&out)) {
		fprintf( stderr, "Не получилось закрыть файл!");
		image_delete(rotated_image);
		image_delete(image);
		return 1;
	}
	image_delete(image);
	image_delete(rotated_image);
	return 0;
}
