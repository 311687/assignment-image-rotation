#include "../include/rotate_image.h"

#include <malloc.h>

static size_t result_position(size_t i, size_t j, size_t width){
    return i * width + j;
}

static size_t source_position(size_t i, size_t j, size_t width, size_t height){
    return (height - 1 - j) * width + i;
}

struct image rotate(struct image const* source) {
    size_t src_pixels = source->width * source->height;
    struct image result = image_create(source->height, source->width);
    result.pixels = malloc(sizeof(struct pixel) * src_pixels);

    for (size_t i = 0; i < source->width; i++) {
        for (size_t j = 0; j < source->height; j++) {
            result.pixels[result_position(i, j, result.width)] = source->pixels[source_position(i, j, source->width, source->height)];
        }
    }

    return result;
}

